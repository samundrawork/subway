<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders}}`.
 */
class m210116_114524_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders}}', [
            'id' => $this->primaryKey(),
            'bread' => $this->integer()->notNull(),
            'bread_size' => $this->string()->notNull(),
            'o_baked' => $this->boolean()->notNull(),
            'sandwich_taste' => $this->integer()->notNull(),
            'extra' => $this->string()->notNull(),
            'vegetables' => $this->json()->notNull(),
            'sauce' => $this->integer()->notNull(),
            'status' => $this->string()->notNull(),
            'client_id' => $this->integer()->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%orders}}');
    }
}
