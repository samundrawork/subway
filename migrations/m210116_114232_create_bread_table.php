<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bread}}`.
 */
class m210116_114232_create_bread_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bread}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bread}}');
    }
}
