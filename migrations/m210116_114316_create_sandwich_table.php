<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sandwich}}`.
 */
class m210116_114316_create_sandwich_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sandwich}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sandwich}}');
    }
}
