<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sauce}}`.
 */
class m210116_114405_create_sauce_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sauce}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sauce}}');
    }
}
