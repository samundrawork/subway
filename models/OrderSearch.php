<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;

/**
 * OrderSearch represents the model behind the search form of `app\models\Orders`.
 */
class OrderSearch extends Orders
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'bread', 'o_baked', 'sandwich_taste', 'sauce', 'client_id'], 'integer'],
            [['bread_size', 'extra', 'vegetables', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bread' => $this->bread,
            'o_baked' => $this->o_baked,
            'sandwich_taste' => $this->sandwich_taste,
            'sauce' => $this->sauce,
            'client_id' => $this->client_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'bread_size', $this->bread_size])
            ->andFilterWhere(['like', 'extra', $this->extra])
            ->andFilterWhere(['like', 'vegetables', $this->vegetables])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
