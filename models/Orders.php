<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $bread
 * @property string $bread_size
 * @property int $o_baked
 * @property int $sandwich_taste
 * @property string $extra
 * @property string $vegetables
 * @property int $sauce
 * @property string $status
 * @property int $client_id
 * @property string $created_at
 * @property string|null $updated_at
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bread', 'bread_size', 'o_baked', 'sandwich_taste', 'extra', 'vegetables', 'sauce'], 'required'],
            [['bread', 'o_baked', 'sandwich_taste', 'sauce', 'client_id'], 'integer'],
            [['vegetables'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['bread_size', 'extra', 'status'], 'string', 'max' => 255],
        ];
    }

    public function beforeSave($insert)
    {

        if ($insert) {
            $this->status = 'open';
            $this->created_at = date('Y-m-d H:i:s');
            $this->updated_at = date('Y-m-d H:i:s');
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bread' => 'Bread',
            'bread_size' => 'Bread Size',
            'o_baked' => 'O Baked',
            'sandwich_taste' => 'Sandwich Taste',
            'extra' => 'Extra',
            'vegetables' => 'Vegetables',
            'sauce' => 'Sauce',
            'status' => 'Status',
            'client_id' => 'Client ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
