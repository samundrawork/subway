<?php

namespace app\models;

use yii\helpers\Url;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $username
 * @property string|null $accessToken
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['username'], 'unique'],
            [['username', 'accessToken'], 'string', 'max' => 255, 'min' => 3],
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->accessToken = \Yii::$app->security->generateRandomString();
        }
        return true;
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['user_access_url']);
    }


    public function afterFind() {
        parent::afterFind();
        $this->user_access_url = Url::base(true).'/order?'.'session='.$this->accessToken;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'accessToken' => 'Access Token',
        ];
    }
}
