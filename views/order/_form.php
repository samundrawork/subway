<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $breadList = ArrayHelper::map(\app\models\Bread::find()->asArray()->all(), 'id', 'name');
    $sandwichList = ArrayHelper::map(\app\models\Sandwich::find()->asArray()->all(), 'id', 'name');
    $vegetableList = ArrayHelper::map(\app\models\Vegetables::find()->asArray()->all(), 'id', 'name');
    $sauceList = ArrayHelper::map(\app\models\Sauce::find()->asArray()->all(), 'id', 'name');
    ?>

    <?= $form->field($model, 'bread')->dropDownList($breadList,
        ['prompt' => '-Choose a Bread Type-']); ?>

    <?= $form->field($model, 'bread_size')->dropDownList(array('15cm' => '15 CM', '30cm' => '30 CM'), ['prompt' => '-Choose a Bread Size-']); ?>

    <?= $form->field($model, 'o_baked')->radioList([0=> 'yes', 1 => 'no',])->label('Oven Baked?') ?>

    <?= $form->field($model, 'sandwich_taste')->dropDownList($sandwichList,
        ['prompt' => '-Choose a Sandwich Type-']); ?>

    <?= $form->field($model, 'extra')->
    dropDownList(array('extra_bacon' => 'Extra bacon', 'double_meat' => 'Double meat ', 'extra_cheese' => 'Extra cheese '), ['prompt' => '-Choose extras-']); ?>

    <label class="control-label">Vegetables</label>

    <?php
    echo Select2::widget([
        'model' => $model,
        'name' => 'vegetables',
        'attribute' => 'vegetables',
        'data' => $vegetableList,
        'options' => [
            'placeholder' => 'Select vegetables ...',
            'multiple' => true
        ],
    ]);
    ?>
    <?= $form->field($model, 'sauce')->dropDownList($sauceList,
        ['prompt' => '-Choose a Sauce Type-']); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
