<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'bread') ?>

    <?= $form->field($model, 'bread_size') ?>

    <?= $form->field($model, 'o_baked') ?>

    <?= $form->field($model, 'sandwich_taste') ?>

    <?php // echo $form->field($model, 'extra') ?>

    <?php // echo $form->field($model, 'vegetables') ?>

    <?php // echo $form->field($model, 'sauce') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'client_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
