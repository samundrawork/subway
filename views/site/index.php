<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Client;
use yii\helpers\Url;


/* @var $this yii\web\View */

$this->title = 'Subway';
?>
<div class="jumbotron">
    <p><a class="btn btn-lg btn-success" href="<?php echo Url::to(['client/index']); ?>">Client</a></p>
    <p class="lead">Add Items</p>
    <p><a class="btn btn-lg btn-success" href="<?php echo Url::to(['bread/index']); ?>">Bread</a>
        <a class="btn btn-lg btn-success" href="<?php echo Url::to(['sandwich/index']); ?>">Sandwich</a>
        <a class="btn btn-lg btn-success" href="<?php echo Url::to(['vegetable/index']); ?>">Vegetable</a>
        <a class="btn btn-lg btn-success" href="<?php echo Url::to(['sauce/index']); ?>">Sauce</a></p>
</div>

<div class="bread-index">

    <?php
    $dataProvider = new ActiveDataProvider([
        'query' => Client::find(),
    ]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
    ]);
    ?>


</div>